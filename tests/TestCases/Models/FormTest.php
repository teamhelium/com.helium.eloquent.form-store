<?php

namespace Helium\FormStore\Tests\TestCases\Models;

use Helium\FormStore\Models\Form;
use Helium\FormStore\Tests\TestCase;

class FormTest extends TestCase
{
    public function testCastsConfigJson()
    {
        /**
         * Test set config
         */
        $config = [
            'abc' => 123
        ];

        $form = Form::create([
            'title' => 'Test Form',
            'config' => $config
        ]);

        $this->assertIsArray($form->config);
        $this->assertIsString($form->config_json);
        $this->assertEquals(json_encode($form->config), $form->config_json);

        /**
         * Test retrieve config
         */
        $form = Form::find($form->id);

        $this->assertIsArray($form->config);
        $this->assertIsString($form->config_json);
        $this->assertEquals(json_encode($form->config), $form->config_json);
    }
}