<?php

namespace Helium\FormStore\Tests\TestCases\Models;

use Helium\FormStore\Models\Form;
use Helium\FormStore\Models\FormSubmission;
use Helium\FormStore\Tests\TestCase;

class FormSubmissionTest extends TestCase
{
    public function testCastsResponseJson()
    {
        /**
         * Test set response
         */
        $response = [
            'abc' => 123
        ];

        $formSubmission = FormSubmission::create([
            'form_id' => factory(Form::class)->create()->id,
            'response' => $response
        ]);

        $this->assertIsArray($formSubmission->response);
        $this->assertIsString($formSubmission->response_json);
        $this->assertEquals(json_encode($formSubmission->response), $formSubmission->response_json);

        /**
         * Test retrieve response
         */
        $formSubmission = FormSubmission::find($formSubmission->id);

        $this->assertIsArray($formSubmission->response);
        $this->assertIsString($formSubmission->response_json);
        $this->assertEquals(json_encode($formSubmission->response), $formSubmission->response_json);
    }
}