<?php

namespace Helium\FormStore\Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
	use SetsUpTests;
}