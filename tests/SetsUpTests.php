<?php

namespace Helium\FormStore\Tests;

use Helium\FormStore\FormStoreServiceProvider;

/**
 * @mixin TestCase
 */
trait SetsUpTests
{
	protected function setUp(): void
	{
		parent::setUp();

		$this->withFactories(__DIR__ . '/../src/database/factories');
		$this->loadMigrationsFrom(__DIR__ . '/../src/database/migrations');
	}

	protected function getPackageProviders($app)
	{
		return [
			FormStoreServiceProvider::class
		];
	}

	protected function getEnvironmentSetUp($app)
	{
		// Setup default database to use sqlite :memory:
		$app['config']->set('database.default', 'testbench');
		$app['config']->set('database.connections.testbench', [
			'driver'   => 'sqlite',
			'database' => ':memory:',
			'prefix'   => '',
		]);
	}
}