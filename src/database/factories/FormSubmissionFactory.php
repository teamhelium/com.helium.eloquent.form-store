<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Helium\FormStore\Models\Form;
use Helium\FormStore\Models\FormSubmission;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(FormSubmission::class,
    function (Faker $faker) {
        return [
            'form_id' => factory(Form::class)->create()->id,
            'response' => json_encode([
                0 => $faker->sentence
            ])
        ];
    });
