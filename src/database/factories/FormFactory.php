<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Helium\FormStore\Models\Form;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Form::class,
    function (Faker $faker) {
        return [
            'title' => $faker->words(3, true),
            'config' => json_encode([
                'questions' => [
                    0 => [
                        'title' => $faker->words(3, true),
                        'description' => $faker->sentence
                    ]
                ]
            ])
        ];
    });
