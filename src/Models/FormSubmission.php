<?php

namespace Helium\FormStore\Models;

use Carbon\Carbon;
use Helium\EloquentInspectable\Contracts\InspectableContract;
use Helium\EloquentInspectable\Traits\Inspectable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FormResponse
 *
 * Attributes
 * --------------------
 * @property int id
 * @property Carbon created_at
 * @property Carbon modified_at
 * @property Carbon deleted_at
 * @property int form_id
 * @property array response
 *
 * Relationships
 * --------------------
 * @property Form form
 */
abstract class FormSubmission extends Model implements InspectableContract
{
    use Inspectable;

	//region Base
    protected $table = 'form_submissions';

	protected $fillable = [
		'form_id',
		'response'
	];

	protected $casts = [
	    'response' => 'array'
    ];

	protected $inspectors = [];
	//endregion

    //region Relationships
	public function form()
	{
		return $this->belongsTo(Form::class);
	}
	//endregion
}