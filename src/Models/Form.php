<?php

namespace Helium\FormStore\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Form
 *
 * Attributes
 * --------------------
 * @property int id
 * @property Carbon created_at
 * @property Carbon modified_at
 * @property Carbon deleted_at
 * @property string title
 * @property string description
 * @property array config
 *
 * Relationships
 * --------------------
 * @property Collection responses
 */
class Form extends Model
{
	//region Base
	protected $fillable = [
		'title',
		'description',
        'config'
	];

	protected $casts = [
	    'config' => 'array'
    ];
	//endregion


    //region Relationships
	public function submissions()
	{
		return $this->hasMany(FormSubmission::class);
	}
	//endregion
}