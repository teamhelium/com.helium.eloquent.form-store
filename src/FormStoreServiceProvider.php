<?php

namespace Helium\FormStore;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class FormStoreServiceProvider extends BaseServiceProvider
{
	public function boot()
	{
        $this->publishes([
            __DIR__ . '/database/factories/' => database_path('factories')
        ], 'factories');

        $this->publishes([
            __DIR__ . '/database/migrations/' => database_path('migrations')
        ], 'migrations');
	}
}